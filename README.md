# Pop- Shop
<p>Proyecto de práctica para el desarrollo Frontend de páginas estáticas con Bootstrap. <br>
Simula una tienda onlide fe Funko Pops, cuenta con las siguientes secciones:
</p>
<ul>
    <li>Home: Vista principal/inicial.</li>
    <li>Producs: Muestra un catalogo de productos disponibles.</li>
    <li>Contact Us: Vista con un formulario donde se ingresan tus datos de contacto.</li>    
</ul>

<h2>El proyecto cuenta con las siguientes características y funcionalidades:</h2>

<ul>
    <li>Desarrollado con estructura HTML.</li>
    <li>Estilos implementados con CSS.</li>
    <li>Bootstrap.</li>    
</ul>

<h5>
    Ahora es posible visualizar la página en ejecución, previamente se ha configurado un website estático con la ayuda de la herramienta GitLab Pages,
    en conjunto  con   GitLab CI y GitLab Runner.
</h5>

### Watch Online
##### Si te gustaría probar la página en ejecución has click [Aquí](https://chichianichi.gitlab.io/pop-shop/index.html "Aquí")
